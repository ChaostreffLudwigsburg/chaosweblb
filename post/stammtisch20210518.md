---
date: "2021-05-18"
tages: ["chaostreff", "veranstaltung"]
title: "Kurzprotokoll 18.05.2021 und Linkliste"
---

* ABF-Substrat-Mangel: https://yyaloo.com/de/index.php/posts/la-carenza-di-substrati-abf-puo-essere-eliminata-non-prima-del-2023 und https://www.igorslab.de/wenn-wichtige-komponenten-zur-mangelware-werden-cpus-gpus-konsolenchips-und-andere-komponenten-als-komplexes-handelsobjekt/
* Kingston A2000-Bug: Es gibt ein Bugfixing-Tool für Linux
* Browsersicherheit: Microsoft Edge unter Linux
* Tool-Idee: Automatisch Aufrufe von Browsern mittracen, Zertifikate in eigener CA erstellen und TLS DPI scannen
* Anregungen zu geplantem Borg-Vortrag: 
    * Probleme mit Hetzner und COW-FS
    * Maintenance-Häufigkeit
    * ssh-Probleme
    * ...
* Erkenntnis: Automatisches Downgrade bei Telekom-Anschlüssen nach mehrfachen Router-Restarts
* Multi-Geiger-Zähler mit ESP32: https://ecocurious.de/projekte/multigeiger-2/ 
* Schulen: Unterrichtsformen während Corona
* Bastelprojekt von M.B. mit https://tixy.land