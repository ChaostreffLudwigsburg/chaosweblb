---
date: "2022-06-10"
tages: ["chaostreff", "veranstaltung"]
title: "Protokoll 10.06.2022"
---

## Interna


Ampoff (Steffen) hatte auf Matrix angekündigt, eine längere Verschnaufpause von der technischen und organisatorischen Federführung unseres Chaostreffs einzulegen, weil er den Aufwand nicht mehr allein bewältigen kann und will. Er zieht den nicht unberechtigten Vergleich mit den letzten "Neustädter Apachen" in [Normaloland](https://www.zdf.de/serien/normaloland/der-stamm-satirische-webserie-100.html).

Das Treffen heute hat den Zweck, ein solches Ende abzuwenden und die Lasten auf mehr Schultern zu verteilen. 

Außer einem neuen Interessierten (der an diesem Abend leider nicht die volle Aufmerksamkeit bekommen hat, sorry nochmal dafür) und Ampoff, sind Buzz (Michael), Fussel (Heiko) und Harvey (Heinz) erschienen und bereit, Aufgaben zu übernehmen. Sie bilden zusammen mit Ampoff das künftige "Team".


### Matrix

Am betreuungsintensivsten (Spam-Abwehr, Updates, Haftung etc.) ist die eigene Matrix-Instanz https://matrix.complb.de. Sie wird stillgelegt, und Chaostreff LB eröffnet auf einem freien Matrix-Server eigene (konsolidierte) Räume. Details folgen.

Das bedeutet auch, dass die Matrix-Useraccounts und privaten Chats auf matrix.complb.de verloren gehen werden. Alle User sind aufgerufen, während einer Übergangsphase über die Export-Funktion z.B. des Element-Clients die Chats zu sichern, die sie archivieren wollen (z.B. als HTML).

Die relevanten offiziellen Chaträume unseres Matrixservers können wahrscheinlich als HTML-Export auf unserem Webserver statisch archiviert werden.


### E-Mail

Die Kontaktadresse chaostreff AT complb PUNKT de wird von einer Weiterleitung zu einem echten Postfach ausgebaut, auf das künftig das ganze "Team" Zugriff hat. Eingehende Anfragen werden verteilt, aber koordiniert beanwortet.


### Webserver

Der eigene Webserver bleibt bestehen, aber die Kosten dafür und für die Domain trägt das "Team" gemeinsam. Das "Team" bekommt Commit-Rechte auf das Codeberg-Repo, in dem die Hugo-Quelltexte gepflegt werden (https://codeberg.org/ChaostreffLudwigsburg/chaosweblb) und aktualisiert diese künftig gemeinsam. Der Umzug zu einem Webhoster ist angesichts der eingespielten Prozesskette und des insgesamt vertretbaren Aufwandes nicht sinnvoll.


### Räume

Es hat nicht nur Nachteile, sich in Kneipen treffen zu müssen :-) Allerdings wäre für interne Vorträge etc. ein kleines, festes Domizil schon sehr wünschenswert. Auch von daher wird der Kontakt mit der Stadtbibliothek weiterverfolgt. Räume im Sinne eines Makerspaces sind beim aktuellen Zuschnitt des Chaostreffs nicht nötig.


### Ausflüge und Veranstaltungen

Vorschläge für Ausflüge (Fussel):

* Interna der Müllverbrennungsanlage Stuttgart (leider nur tagsüber freitags möglich)
* Interna der Trinkwasserversorgung Stuttgart
* Aufzu-Test-Turm in Rottweil

Vorschläge für Vorträge (Ampoff):

* E-Mail-Server-Konfiguration
* Intrusion Detection und Abwehrmaßnahmen
* Einen Demo-Webserver live auf Schwachstellen scannen und hacken


### Chaostreff

Nach erfolgreicher Verteilung der Aufgaben sind sich alle einig, dass wir den Status eines Chaostreffs behalten und nicht auf einen noch unverbindlicheren Stammtisch zurückfallen wollen.

Der Text auf https://www.ccc.de/de/club/chaostreffs muss aktualisiert werden.


## Andere Themen

Es wurden nicht nur Interna besprochen. Andere Themen:

* Rückblick Gulaschprogrammiernacht in Karlsruhe
    * Gute Aktion, gerne wiederholen
    * Vorträge im Vergleich zum Congress ertragreicher für die eigene Praxis
* Coding-Styles: PEP, Clear Code
* Codeberg: Wie kann man geforkte Repos nach längerer Zeit wieder mit dem Upstream-Stand aktualisieren? Es scheint im Web-GUI von Gitea (im Gegensatz zu Github oder Gitlab) keine Funktion dafür zu geben.
* Chaos Communication Camp (https://events.ccc.de/camp/)
    * Harvey schwärmt davon seit 2007 und zeigt Fotos von 2019
    * Gemeinsamer Besuch 2023 wird geplant
    * Ort wieder Mildenberg? Ist noch unklar.
