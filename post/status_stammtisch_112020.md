---
date: "2020-10-28"
tages: ["chaostreff"]
title: "Status Chaostreff ab Oktober 2020"
---


Der Chaostreff Ludwigsbur|r|g trifft sich an jedem ersten Mittwoch im Monat zusammen mit der LuLUG bei Jitsi: https://linuxwiki.de/LugLudwigsburg/Treffen

An jedem dritten Dienstag im Monat treffen wir uns in unserem Jitsi: https://meet.jit.si/chaoslb