---
title: ""
icon: "fa-group"
weight: 1
---

<img src="logo.jpeg" class="center" width="300" height="300">


#### Wann und wo trefft ihr euch?

<!--
Das nächste Treffen findet am xx.xx.2022 ab 19:30 online unter https://meet.jit.si/chaostrefflb0001 statt.

Das nächste Treffen findet statt am Donnerstag, dem xx. MMM 2023 ab 18:30 Uhr im [Blauen Engel](https://www.blauerengel-ludwigsburg.de/) in Ludwigsburg. 
-->

<!-- Das heutige Treffen (Donnerstag, **25. April 2024 ab 18:30 Uhr** im [Blauen Engel](https://www.blauerengel-ludwigsburg.de/) in Ludwigsburg **ist unsicher**. -->

Das nächste Treffen findet ausnahmsweise statt am Mittwoch, dem 26. Februar 2025 ab 19:30 Uhr im [DemoZ Ludwigsburg](https://demoz-lb.de/). Mehr Infos dazu auf [Veranstaltungen](https://complb.de/vas/)

Wir treffen uns in der Regel am letzten Donnerstag im Monat ab 18:00 Uhr im [Towers Pub](https://towerspub.com/) in Ludwigsburg und ständig bei Matrix https://matrix.to/#/#chaostreff-lb:matrix.org 

Ihr erreicht uns außerdem per Mail über chaostreff AT complb PUNKt de.

Im Rahmen des Chaostreffs gibt es auch gelegentlich Kurzvorträge oder Ausflüge, mehr Infos dazu unter https://complb.de/vas.


#### Um was gehts?

Der Chaostreff Ludwigsburg ist ein lockeres und offenes Zusammentreffen von Menschen, die sich dem CCC und der [Hackerethik](https://www.ccc.de/de/hackerethik) nahe fühlen.

Neugierige und Interessierte, die sich für Themen rund um Technik und Datenschutz interessieren, sind uns stets willkommen!